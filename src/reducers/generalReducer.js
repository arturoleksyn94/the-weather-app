import { handleActions } from 'redux-actions';
import immutable from 'immutability-helper';

import { STATUS, ActionTypes } from '../constants';

export const generalState = {
  status: STATUS.IDLE,
  choosenCity: null,
  savedCities: []
};

export default {
  generalReducer: handleActions(
    {
      [ActionTypes.GET_WEATHER]: state =>
        immutable(state, {
          status: { $set: STATUS.RUNNING },
        }),
      [ActionTypes.GET_WEATHER_SUCCESS]: (state, { payload }) =>
        immutable(state, {
          status: { $set: STATUS.READY },
          choosenCity: { $set: payload },
        }),
      [ActionTypes.GET_WEATHER_FAILURE]: (state) =>
        immutable(state, {
          status: { $set: STATUS.ERROR },
        }),

      [ActionTypes.TOGGLE_FAVOURITE_CITY_SUCCESS]: (state, { payload: { data, save } }) => {
        let copy_array = state.savedCities ? [...state.savedCities] : [];

        if (save) {
          copy_array.push(data)
        } else {
          copy_array.splice(copy_array.findIndex((item) => item.id === data.id), 1)
        }

        return (
          immutable(state, {
            savedCities: { $set: copy_array },
          })
        )
      },

      [ActionTypes.UPDATE_WEATHER_CITIES]: state =>
        immutable(state, {
          status: { $set: STATUS.RUNNING },
        }),
      [ActionTypes.UPDATE_WEATHER_CITIES_SUCCESS]: (state, { payload }) =>
        immutable(state, {
          status: { $set: STATUS.READY },
          savedCities: { $set: payload },
        }),
      [ActionTypes.UPDATE_WEATHER_CITIES_FAILURE]: (state) =>
        immutable(state, {
          status: { $set: STATUS.ERROR },
        }),
    },
    generalState,
  ),
};