import React, { Component } from 'react';
import '../../theme/layout.scss';
import '../../theme/wrapper.scss';
import '../../theme/template.scss';

class Layout extends Component {
  render() {
    return (
      <div className="content">
        <section>
          <h2>Преимущества наших услуг</h2>
          <div className="row">
            <div className="block-items">
              <div>
                <div className="block">
                  <img
                    alt="bill"
                    src={require('../../resources/icons/bill.png')}
                    className="icon"
                  />
                  <p>Lorem Ipsum используют потому, что тот обеспечивает</p>
                </div>
                <div className="block">
                  <img
                    alt="fire"
                    src={require('../../resources/icons/fire.png')}
                    className="icon"
                  />
                  <p>Многие думают, что Lorem Ipsum - взятый с потолка</p>
                </div>
                <div className="block">
                  <img
                    alt="sim"
                    src={require('../../resources/icons/sim.png')}
                    className="icon"
                  />
                  <p>В результате сгенерированный Lorem Ipsum выглядит</p>
                </div>
              </div>
              <div>
                <div className="block">
                  <img
                    alt="feedback"
                    src={require('../../resources/icons/feedback.png')}
                    className="icon"
                  />
                  <p>За прошедшие годы текст Lorem Ipsum получил много</p>
                </div>
                <div className="block">
                  <img
                    alt="imac"
                    src={require('../../resources/icons/imac.png')}
                    className="icon"
                  />
                  <p>Также все другие известные генераторы Lorem Ipsum</p>
                </div>
                <div className="block">
                  <img
                    alt="ethernet"
                    src={require('../../resources/icons/ethernet.png')}
                    className="icon"
                  />
                  <p>Этот трактат по теории этики был очень популярен</p>
                </div>
              </div>
            </div>
            <div className="block-comment">
              <p className="comment">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации</p>
              <div className="comment-author">
                <img
                  alt="author"
                  src={require('../../resources/images/avatar.png')}
                  className="author-avatar"
                />
                <span className="author-possition">Комментарий представителя компании</span>
                <span className="author-name">Бондарь Анастасия</span>
              </div>
            </div>
          </div>
        </section>

        <section>
          <h2 className="not-full-title"><b>Есть много вариантов Lorem Ipsum</b>, но большинство из них имеет не всегда приемлемые</h2>
          <p className="not-full-title">Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором.</p>
          <div className="detail-block">
            <h3><b>Lorem Ipsum</b> Dolore</h3>
            <div className="row">
              <div className="image-wrapper left">
                <img
                  alt="image_1"
                  src={require('../../resources/images/image_1.png')}
                />
              </div>
              <div className="detail-content">
                <div className="detail-accent">
                  <div>
                    <span className="label">Цена от</span>
                    <span className="value">$850</span>
                  </div>
                  <div>
                    <span className="label">Сроки от</span>
                    <span className="value">6 месяцев</span>
                  </div>
                  <div>
                    <span className="label">Основания</span>
                    <span className="label no-margin">Есть много вариантов Lorem Ipsum, но большинство</span>
                  </div>
                </div>
                <h4>Также все другие известные</h4>
                <ul>
                  <li>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые</li>
                  <li>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает</li>
                  <li>В результате сгенерированный Lorem Ipsum выглядит правдоподобно</li>
                  <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="detail-block">
            <h3><b>Lorem Ipsum</b> Dolore</h3>
            <div className="row sm-reverse">
              <div className="detail-content">
                <div className="detail-accent">
                  <div>
                    <span className="label">Цена от</span>
                    <span className="value">$850</span>
                  </div>
                  <div>
                    <span className="label">Сроки от</span>
                    <span className="value">6 месяцев</span>
                  </div>
                  <div>
                    <span className="label">Основания</span>
                    <span className="label no-margin">Есть много вариантов Lorem Ipsum, но большинство</span>
                  </div>
                </div>
                <h4>Также все другие известные</h4>
                <ul>
                  <li>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые</li>
                  <li>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает</li>
                  <li>В результате сгенерированный Lorem Ipsum выглядит правдоподобно</li>
                  <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов</li>
                </ul>
              </div>
              <div className="image-wrapper right">
                <img
                  alt="image_1"
                  src={require('../../resources/images/image_2.png')}
                />
              </div>
            </div>

          </div>
        </section>
      </div>
    )
  }
}


export default Layout;
