
const styles = theme => ({
  logoTitle: {
    flexGrow: 1
  },
  container: {
    height: '100vh',
    paddingTop: '10vh'
  },
  content: {
    paddingTop: '5vh'
  },
  paper: {
    padding: 20,
    position: 'relative',
    marginBottom: 10
  },
  iconWrapper: {
    display: 'flex',
    alignItems: 'center'
  },
  saveIcon: {
    position: 'absolute',
    top: 10,
    right: 10
  },
  searchWrapper: {
    position: 'relative',
  },
  searchIcon: {
    position: 'absolute',
    top: 12,
    left: 15
  },
  subTitle: {
    fontSize: '15px', color: '#ddd', marginRight: 5
  },
  title: {
    marginBottom: 20, 
    position: 'relative'
  },
  reloadIcon: {
    position: 'absolute',
    right: 0,
    top: -8
  }
});

export default styles;