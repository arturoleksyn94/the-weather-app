// @flow
/**
 * @module Actions/User
 * @desc User Actions
 */
import { createActions } from 'redux-actions';

import { ActionTypes } from '../constants';

export const { 
  getWeather,
  updateWeatherCities,
  toggleFavouriteCity, 
} = createActions({
  [ActionTypes.GET_WEATHER]: (city_name) => ({city_name}),
  [ActionTypes.UPDATE_WEATHER_CITIES]: () => ({}),
  [ActionTypes.TOGGLE_FAVOURITE_CITY]: (data, save) => ({data, save}),
});