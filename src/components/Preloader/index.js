import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import styles from './styles';

class Preloader extends React.PureComponent {
  render() {
    const { classes } = this.props;

    return (
      <Typography component="div" className={classes.container}>
        <Typography variant="h4" align="center">
          Loading...
        </Typography>
      </Typography>
    );
  }
}

Preloader.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(Preloader));