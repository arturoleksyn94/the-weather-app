import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { CssBaseline, Typography, Container, AppBar, Toolbar, Grid, Avatar, Paper, IconButton } from '@material-ui/core';
import { Favorite, FavoriteBorder, CloseSharp, SearchSharp, Loop } from '@material-ui/icons';
import withStyles from '@material-ui/core/styles/withStyles';
import { getWeather, toggleFavouriteCity, updateWeatherCities } from '../../actions/general';
import styles from './styles';
import 'react-google-places-autocomplete/dist/assets/index.css';
import './keyframes.css';


class Home extends Component {

  calcAverage = (cities) => {
    return Math.round((cities.reduce((sum, item) => { return sum + item.main.temp }, 0)) / cities.length)
  }

  renderListItemCity = (city) => {
    const { classes, toggleFavouriteCity } = this.props;

    return (
      <Paper
        key={city.id}
        elevation={1}
        className={classes.paper}
      >
        <Typography variant="h6" style={{ display: 'inline-block', marginRight: 15 }}>
          {city.name}
        </Typography>
        <Typography variant="inherit" >
          <b>{Math.round(city.main.temp)} °C</b>
        </Typography>
        <IconButton
          aria-label="favourite"
          className={classes.saveIcon}
          onClick={() => toggleFavouriteCity(city, false)}
        >
          <CloseSharp style={{ color: "gray" }} />
        </IconButton>
      </Paper>
    )
  }

  render() {
    const { classes, getWeather, choosenCity, savedCities, toggleFavouriteCity, updateWeatherCities } = this.props;
    const isSaved = (choosenCity && savedCities) ? savedCities.find((item) => item.id === choosenCity.id) : false;

    return (
      <React.Fragment>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" noWrap className={classes.logoTitle}>
              The Weather App
            </Typography>
            <Typography component="div" style={{ display: 'flex' }}>
              {
                savedCities && savedCities.length ? (
                  <React.Fragment>
                    <Typography component="span" className={classes.subTitle}>
                      Average temperature in your favorite cities
                    </Typography>
                    <Typography component="p">
                      {this.calcAverage(savedCities)} °C
                    </Typography>
                  </React.Fragment>
                ) : <React.Fragment></React.Fragment>
              }
            </Typography>
          </Toolbar>
        </AppBar>
        <Container fixed>
          <Typography component="div" className={classes.container}>
            <Typography component="div" className={classes.searchWrapper}>
              <GooglePlacesAutocomplete
                inputStyle={{ width: '100%', paddingLeft: 50, height: 50 }}
                placeholder={'Search...'}
                onSelect={(city) => getWeather(city.description)}
              />
              <SearchSharp className={classes.searchIcon} style={{ color: "gray" }} />
            </Typography>

            <Grid container spacing={3} className={classes.content}>
              <Grid item xs={12} md={7}>
                <Typography variant="h5" className={classes.title}>
                  The Weather Today
                </Typography>
                {
                  choosenCity && (
                    <Paper elevation={1} className={classes.paper}>
                      <Typography variant="h4" >
                        {choosenCity.name}
                      </Typography>
                      {
                        choosenCity.weather[0] && (
                          <div className={classes.iconWrapper}>
                            <Avatar alt={choosenCity.weather[0].main} src={`http://openweathermap.org/img/w/${choosenCity.weather[0].icon}.png`} className={classes.icon} />
                            <span>{choosenCity.weather[0].main}</span>
                          </div>
                        )
                      }
                      <Typography variant="subtitle1" >Temperature, <b>{Math.round(choosenCity.main.temp)} °C</b></Typography>
                      <Typography variant="subtitle1" >Feels like, <b>{Math.round(choosenCity.main.feels_like)} °C</b></Typography>
                      <Typography variant="subtitle1" >Pressure, <b>{Math.round(choosenCity.main.pressure)} hPa</b></Typography>
                      <Typography variant="subtitle1" >Humidity, <b>{Math.round(choosenCity.main.humidity)} %</b></Typography>
                      <Typography variant="subtitle1" >Minimum temperature at the moment, <b>{Math.round(choosenCity.main.temp_min)} °C</b></Typography>
                      <Typography variant="subtitle1" >Maximum temperature at the moment, <b>{Math.round(choosenCity.main.temp_max)} °C</b></Typography>
                      <IconButton
                        aria-label="favourite"
                        className={classes.saveIcon}
                        onClick={() => toggleFavouriteCity(choosenCity, !isSaved)}
                      >
                        {
                          isSaved ? <Favorite style={{ color: "red" }} /> : <FavoriteBorder style={{ color: "gray" }} />
                        }
                      </IconButton>
                    </Paper>
                  )
                }

              </Grid>
              <Grid item xs={12} md={5}>
                <Typography variant="h5" className={classes.title}>
                  Favorite Cities
                  {
                    savedCities && savedCities.length ? (
                      <IconButton
                        aria-label="reload"
                        className={classes.reloadIcon}
                        onClick={() => updateWeatherCities()}
                      >
                        <Loop style={{ color: "gray" }} />
                      </IconButton>
                    ): <div/>
                  }
                </Typography>
                {
                  savedCities.map((city) => this.renderListItemCity(city))
                }
              </Grid>
            </Grid>
          </Typography>
        </Container>
      </React.Fragment>
    )
  }
}

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};


/* istanbul ignore next */
const mapStateToProps = (state) => ({
  choosenCity: state.generalReducer.choosenCity,
  savedCities: state.generalReducer.savedCities || []
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getWeather,
    updateWeatherCities,
    toggleFavouriteCity
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Home));
