import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Home from './pages/home';
import Layout from './pages/layout';
import ErrorDrawer from './components/ErrorDrawer';
import Preloader from './components/Preloader';
import history from './modules/history';
import { STATUS } from './constants';
import './App.css';

const Main = styled.main`
  flex: 1;
  align-items: stretch;
  display: flex;
`;

export class App extends React.Component {
  render() {
    const { dispatch, isRequest } = this.props;

    return (
      <Router history={history}>
        <ErrorDrawer dispatch={dispatch}/> 
        {isRequest === STATUS.RUNNING && <Preloader/>}
        <Main isAuthenticated>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/layout' component={Layout} />
          </Switch>
        </Main>
      </Router>
    );
  }
}

const mapStateToProps = (state) => ({
  isRequest: state.generalReducer.status,
})

export default connect(mapStateToProps)(App);
