import generalReducer from './generalReducer';
import errorReducer from './errorReducer';

export default {
  ...generalReducer,
  ...errorReducer
};