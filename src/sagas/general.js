/**
 * @module Sagas/User
 * @desc User
 */

import { all, call, put, takeLatest, select } from 'redux-saga/effects';
import { request } from '../modules/client';
import config from '../config';
import { ActionTypes } from '../constants';

const getSavedCities = (state) => state.generalReducer.savedCities;

export function* getWeather({payload: { city_name = '' }}) {
  try {
    const response = yield call(
      request,
      `weather`,
      {
        method: 'GET',
        params: { 
          q: city_name,
          appid: config.weather_app_id,
          units: 'metric'
        }
      }
    );
    yield put({
      type: ActionTypes.GET_WEATHER_SUCCESS,
      payload: response,
    });
    
  } catch (err) {
    /* istanbul ignore next */
    yield put({
      type: ActionTypes.GET_WEATHER_FAILURE,
      payload: err,
    });
    if (err.data && err.data.message ) (
      yield put({
        type: ActionTypes.SET_ERROR_MESSAGE,
        payload: {message: err.data.message},
      })
    )
  }
}

export function* getWeatherCities() {
  const savedCities = yield select(getSavedCities);
  try {
    const callList = savedCities.map((item) => {
      return call(
        request,
        `weather`,
        {
          method: 'GET',
          params: { 
            id: item.id,
            appid: config.weather_app_id,
            units: 'metric'
          }
        }
      )
    })
    const response = yield all(callList)

    yield put({
      type: ActionTypes.UPDATE_WEATHER_CITIES_SUCCESS,
      payload: response,
    });
    
  } catch (err) {
    /* istanbul ignore next */
    yield put({
      type: ActionTypes.UPDATE_WEATHER_CITIES_FAILURE,
      payload: err,
    });
    if (err.data && err.data.message ) (
      yield put({
        type: ActionTypes.SET_ERROR_MESSAGE,
        payload: {message: err.data.message},
      })
    )
  }
}


export function* toggleFavouriteCity({payload}) {
  const savedCities = yield select(getSavedCities);

  if (payload.save && savedCities.length >= 5) {
    yield put({
      type: ActionTypes.SET_ERROR_MESSAGE,
      payload: {message: "You can't save more than 5 cities("},
    });
    return;
  } 
  yield put({
    type: ActionTypes.TOGGLE_FAVOURITE_CITY_SUCCESS,
    payload,
  });
}


/**
 * General Sagas
 */
export default function* root() {
  yield all([
    takeLatest(ActionTypes.GET_WEATHER, getWeather),
    takeLatest(ActionTypes.UPDATE_WEATHER_CITIES, getWeatherCities),
    takeLatest(ActionTypes.TOGGLE_FAVOURITE_CITY, toggleFavouriteCity),
  ]);
}
