import keyMirror from 'fbjs/lib/keyMirror';

/**
 * @namespace Constants
 * @desc App constants
 */

/**
 * @constant {Object} ActionTypes
 * @memberof Constants
 */
export const ActionTypes = keyMirror({
  /** General*/
  GET_WEATHER: 'GET_WEATHER',
  GET_WEATHER_SUCCESS: 'GET_WEATHER_SUCCESS',
  GET_WEATHER_FAILURE: 'GET_WEATHER_FAILURE',

  TOGGLE_FAVOURITE_CITY: 'TOGGLE_FAVOURITE_CITY',
  TOGGLE_FAVOURITE_CITY_SUCCESS: 'TOGGLE_FAVOURITE_CITY_SUCCESS',
  TOGGLE_FAVOURITE_CITY_FAILURE: 'TOGGLE_FAVOURITE_CITY_FAILURE',

  UPDATE_WEATHER_CITIES: 'UPDATE_WEATHER_CITIES',
  UPDATE_WEATHER_CITIES_SUCCESS: 'UPDATE_WEATHER_CITIES_SUCCESS',
  UPDATE_WEATHER_CITIES_FAILURE: 'UPDATE_WEATHER_CITIES_FAILURE',

  /** Error*/
  TOGGLE_ERROR_POPUP: 'TOGGLE_ERROR_POPUP',
  SET_ERROR_MESSAGE: 'SET_ERROR_MESSAGE',
});

/**
 * @constant {Object} STATUS
 * @memberof Constants
 */
export const STATUS = {
  IDLE: 'idle',
  RUNNING: 'running',
  READY: 'ready',
  SUCCESS: 'success',
  ERROR: 'error',
};