
import { all, fork } from 'redux-saga/effects';

import general from './general';

/**
 * rootSaga
 */
export default function* root() {
  yield all([fork(general)]);
}