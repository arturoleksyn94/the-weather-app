const styles = theme => ({
  errorText: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
    margin: 20
  },
  errorBlock: {
    backgroundColor: 'red'
  },
  background: {
    backgroundColor:'transparent'
  }
});

export default styles;